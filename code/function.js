// Реалізуйте клас Worker (Працівник), який матиме такі властивості: name (ім'я), surname (прізвище),
// rate (ставка за день роботи), days (кількість відпрацьованих днів).
// Також клас повинен мати метод getSalary(), який виводитиме зарплату працівника.
// Зарплата - це добуток (множення) ставки rate на кількість відпрацьованих днів days.

class Worker {
   constructor(name, surname, rate, days) {
      this.name = name;
      this.surname = surname;
      this.rate = rate;
      this.days = days;      
   }
   getSalary(){
       return `Зарплата у ${this.surname} ${this.name} становить ` + this.rate*this.days +`$`;
   }
}

let worker = new Worker('Іван', 'Іванов', 300, 4);

document.write(worker.getSalary());

document.write(`<hr/>`);

// Реалізуйте клас MyString, який матиме такі методи: метод reverse(),
// який параметром приймає рядок, а повертає її в перевернутому вигляді, метод ucFirst(),
// який параметром приймає рядок, а повертає цей же рядок, зробивши його першу літеру великою
// та метод ucWords, який приймає рядок та робить заголовною першу літеру кожного слова цього рядка.

class MyString {
   constructor(str) {
      this.str = str;
   }
   reverse(){
      let arr = this.str.split('');
      let newArr=[];
      for (let i=arr.length; i>=0; i--){
         newArr.push(arr[i]);
      }
      return newArr.join('');
   }
   ucFirst(){
      return this.str[0].toUpperCase() + this.str.slice(1);
   }
   ucWords(){
      let arr = this.str.split('');
      let newArr=[];
      for (let i=0; i<arr.length; i++){
         if(i===0){
            arr[i]=arr[i].toUpperCase();
         }
         if(arr[i]===' '){
            arr[i+1]=arr[i+1].toUpperCase();
         }
         newArr.push(arr[i]);
      }
      return newArr.join('');
   }
}

let str = new MyString('приВет Мир! hello world!');

document.write(str.reverse());

document.write(`<hr/>`);

document.write(str.ucFirst());

document.write(`<hr/>`);

document.write(str.ucWords());

document.write(`<hr/>`);

// Створіть клас Phone, який містить змінні number, model і weight.
// Створіть три екземпляри цього класу.
// Виведіть на консоль значення їх змінних.
// Додати в клас Phone методи: receiveCall, має один параметр - ім'я. Виводить на консоль повідомлення "Телефонує {name}". Метод getNumber повертає номер телефону. Викликати ці методи кожного з об'єктів.

class Phone {
   constructor(number, model, weight, name) {
      this.number = number;
      this.model = model;
      this.weight = weight;
      this.name = name;
   }
   receiveCall(){      
      return `Телефонує ${this.name}`;
   }
   getNumber(){
      return this.number;
   }
}

let phones = [
   new Phone(80501234567, 'iPhone 13', 150, 'Миколай'),
   new Phone(80665671111, 'Samsung Galaxy S21', 200, 'Іван'),
   new Phone(80989996611, 'iPhone 14', 270, 'Євген'),
]

for (let el of phones) {
   console.log(`${el.number} - ${el.model} - ${el.weight}грм.`);
   console.log(el.receiveCall());
   console.log(el.getNumber());
}

// Створити клас Car , Engine та Driver.
// Клас Driver містить поля - ПІБ, стаж водіння.
// Клас Engine містить поля – потужність, виробник.
// Клас Car містить поля – марка автомобіля, клас автомобіля, вага, водій типу Driver, мотор типу Engine. Методи start(), stop(), turnRight(), turnLeft(), які виводять на друк: "Поїхали", "Зупиняємося", "Поворот праворуч" або "Поворот ліворуч". А також метод toString(), який виводить повну інформацію про автомобіль, її водія і двигуна.

// Створити похідний від Car клас - Lorry (вантажівка), що характеризується також вантажопідйомністю кузова.
// Створити похідний від Car клас - SportCar, який також характеризується граничною швидкістю.

function Car(marka, carClass, weight) {
   this.marka = marka;
   this.carClass = carClass;
   this.weight = weight;
   this.engine = new Engine(180, 'BMW corp');
   this.driver = new Driver('Дин Джонс', 40);
   this.start = function () {
      document.write("Поїхали");
   }
   this.stop = function () {
      document.write("Зупиняємося");
   }
   this.turnRight = function () {
      document.write("Поворот праворуч");
   }
   this.turnLeft = function () {
      document.write("Поворот ліворуч");
   }
   this.toString = function () {
      document.write(`<h3>Автомобіль</h3>
      <p>марка автомобіля - ${this.marka}</p>
      <p>клас автомобіля - ${this.carClass}</p>
      <p>вага - ${this.weight} кг</p>
      
      <h3>Водій</h3>
      <p>ПІБ водія - ${this.driver.fullName}</p>
      <p>стаж водіння - ${this.driver.drivingExperience} років</p>
      
      <h3>Двигун</h3>
      <p>потужність - ${this.engine.power} к.с.</p>
      <p>виробник - ${this.engine.company}</p>
      <hr/>`);
   }
}

function Engine(power, company) {
      this.power = power;
      this.company = company;
}

function Driver(fullName, drivingExperience) {
      this.fullName = fullName;
      this.drivingExperience = drivingExperience;
}

function Lorry (carryingCapacity) {
   this.carryingCapacity = carryingCapacity;
}

function SportCar(maxSpeed) {
   this.maxSpeed = maxSpeed;
}

var car = new Car('BMW', 'X5', 2500);

car.toString();

Lorry.prototype = car;

var lorry = new Lorry(7000);

SportCar.prototype = car;

var sportCar = new SportCar(420);

// Створити клас Animal та розширюючі його класи Dog, Cat, Horse.
// Клас Animal містить змінні food, location і методи makeNoise, eat, sleep. Метод makeNoise, наприклад, може виводити на консоль "Така тварина спить".
// Dog, Cat, Horse перевизначають методи makeNoise, eat.
// Додайте змінні до класів Dog, Cat, Horse, що характеризують лише цих тварин.
// Створіть клас Ветеринар, у якому визначте метод void treatAnimal(Animal animal). Нехай цей метод роздруковує food і location тварини, що прийшла на прийом.
// У методі main створіть масив типу Animal, в який запишіть тварин всіх типів, що є у вас. У циклі надсилайте їх на прийом до ветеринара.

function Animal(food, location) {
   this.food = food;
   this.location = location;
   this.makeNoise = function () {
      console.log("Така тварина спить");
   }
   this.eat = function () {
      console.log("Така тварина їсть");
   }
   this.sleep = function () {
      console.log("Така тварина спить");
   }
}

function Dog(nickname, food, location) {
   this.className = 'Пес';
   this.nickname = nickname;
   this.food = food;
   this.location = location;
   this.makeNoise = function () {
      console.log(`${this.nickname} - гавкає`);
   }
   this.eat = function () {
      console.log(`${this.nickname} їсть ${this.food}.`);
   }
}

function Cat(nickname) {
   this.className = 'Кіт';
   this.nickname = nickname;
   this.makeNoise = function () {
      console.log(`${this.nickname} - нявкає`);
   }
   this.eat = function () {
      console.log(`${this.nickname} їсть ${this.food}.`);
   }
}

function Horse(nickname, food, location) {
   this.className = 'Кінь';
   this.nickname = nickname;
   this.food = food;
   this.location = location;
   this.makeNoise = function () {
      console.log(`${this.nickname} - фирчить`);
   }
   this.eat = function () {
      console.log(`${this.nickname} їсть ${this.food}.`);
   }
}

function Veterinarian(animals) {
   this.animals = animals;
   this.treatAnimal = function () {
      animals.forEach(animal => {
         document.write(`<p>${animal.nickname}. Їжа - ${animal.food}. Живе у ${animal.location}.</p>`);
      });      
   }
   this.main = function () {
      animals.forEach(animal => {
         document.write(`<p>Запрошується на прийом до ветеринара ${animal.className} - ${animal.nickname}.</p>`);
      }); 
   }
}

var animal = new Animal('Wiskas', 'квартирі');

Dog.prototype = animal;
Cat.prototype = animal;
Horse.prototype = animal;

Veterinarian.prototype = animal;

var dog = new Dog('Бобік', 'Pedigree', 'будці');
var cat = new Cat('Мурзик');
var horse = new Horse('Черниш', 'Сіно', 'стайні');

var animals = [dog, cat, horse];

var veterinarian = new Veterinarian(animals);
veterinarian.treatAnimal();
document.write('<hr/>');
veterinarian.main();